# MeFit API

## Description

A Web API for the MeFit Mini Case. Has CRUD Endpoints for the Profile Class and persist data using Entity Framework.

- To seed dummy data make a post request to endpoint: /api/SeedDatabase.
- Make sure to change the connection string in appsettings.json and make your own migration

Application Type: ASP.NET Core Web API

## Contributors

Cem Pedersen     @cemsep     https://gitlab.com/cemsep

Jonas Horvli     @jonas.ellefsen.horvli     https://gitlab.com/jonas.ellefsen.horvli
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MeFitAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly MeFitContext _context;

        public ProfileController(MeFitContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Profile>> GetProfiles()
        {
            return _context.Profiles;
        }


        [HttpGet("{id}")]
        public ActionResult<Profile> GetProfileById(int id)
        {
            return _context.Profiles
                .Include(p => p.User)
                .Include(p => p.Address)
                .Include(p => p.Program)
                .Include(p => p.Workout)
                .Include(p => p.Set)
                .SingleOrDefault(p => p.Id == id);
        }

        [HttpPost]
        public ActionResult<Profile> PostProfile(Profile profile)
        {
            try {
                _context.Profiles.Add(profile);
                _context.SaveChanges();
            } catch (Exception exc)
            {
                return BadRequest(exc);
            }

            return CreatedAtAction("GetProfileById", new Profile { Id = profile.Id }, profile);
        }

        [HttpPut("{id}")]
        public ActionResult<Profile> PutProfile(int id, Profile profile)
        {
            if (id != profile.Id)
            {
                return BadRequest();
            }
            _context.Entry(profile).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<Profile> DeleteProfile(int id)
        {
            Profile profile = _context.Profiles.Find(id);

            if (profile == null)
            {
                return NotFound();
            }

            _context.Profiles.Remove(profile);
            _context.SaveChanges();

            return profile;
        }
    }
}